#ifndef __PRINT_HELPERS_HPP
#define __PRINT_HELPERS_HPP

#include <iomanip>
#include <istream>
#include <ostream>
#include <iostream>
#include <cstring>
#include <string>

extern "C" {
#include "fp12e.h"
#include "fp6e.h"
#include "fp2e.h"
#include "fpe.h"
}

class BinarySizeT {
    public:
        BinarySizeT()
        { /* */ }

        BinarySizeT(size_t data)
            : data(data)
        { /* */ }

        void set(size_t in)
        { data = in; }

        size_t val() const
        { return data; }

        friend std::ostream& operator<<(std::ostream& os, const BinarySizeT& output);
        friend std::istream& operator>>(std::istream& is, BinarySizeT& input);

    private:
        size_t data;
};

class BinaryBool {
    public:
        BinaryBool()
        { /* */ }
        
        BinaryBool(bool data)
            : data(data)
        { /* */ }

        void set(bool in)
        { data = in; }

        bool val() const
        { return data; }

        friend std::ostream& operator<<(std::ostream& os, const BinaryBool& output);
        friend std::istream& operator>>(std::istream& is, BinaryBool& input);

    private:
        bool data;
};

class Fp12e {
    public:
        Fp12e();
        Fp12e(const fp12e_t& input);
        friend std::ostream& operator<<(std::ostream& os, const Fp12e& output);
        friend std::istream& operator>>(std::istream& is, Fp12e& input);
        fp12e_t data;
};

class Fp6e {
    public:
        Fp6e();
        Fp6e(const fp6e_t& input);
        friend std::ostream& operator<<(std::ostream& os, const Fp6e& output);
        friend std::istream& operator>>(std::istream& is, Fp6e& input);
        fp6e_t data;
};

class Fp2e {
    public:
        Fp2e();
        Fp2e(const fp2e_t& input);
        friend std::ostream& operator<<(std::ostream& os, const Fp2e& output);
        friend std::istream& operator>>(std::istream& is, Fp2e& input);
        fp2e_t data;
};

class Fpe {
    public:
        Fpe();
        Fpe(const fpe_t& input);
        friend std::ostream& operator<<(std::ostream& os, const Fpe& output);
        friend std::istream& operator>>(std::istream& is, Fpe& input);
        fpe_t data;
};

std::ostream& hex_double(std::ostream& os, double d);
std::istream& hex_double(std::istream& is, double& d);
std::ostream& raw_double(std::ostream& os, double d);
std::istream& raw_double(std::istream& is, double& d);

#endif
