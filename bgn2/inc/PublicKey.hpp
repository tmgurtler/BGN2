#ifndef __BGN_PUBLICKEY_HPP
#define __BGN_PUBLICKEY_HPP

#include "Scalar.hpp"
#include "Bipoint.hpp"
#include "Quadripoint.hpp"
#include "pairing.hpp"

class BGNPublicKey
{
	public:
		BGNPublicKey();
		BGNPublicKey(const BGNPublicKey& other);
		
		void encrypt(CurveBipoint& G_element, const Scalar& cleartext) const;
		void encrypt(TwistBipoint& H_element, const Scalar& cleartext) const;
		void encrypt(CurveBipoint& G_element, TwistBipoint& H_element, const Scalar& cleartext) const;
		CurveBipoint curveEncrypt(Scalar& lambda, const Scalar& cleartext) const;
		TwistBipoint twistEncrypt(Scalar& lambda, const Scalar& cleartext) const;

		CurveBipoint homomorphic_addition(const CurveBipoint& a, const CurveBipoint& b) const;
		TwistBipoint homomorphic_addition(const TwistBipoint& a, const TwistBipoint& b) const;
		Quadripoint homomorphic_addition(const Quadripoint& a, const Quadripoint& b) const;
		Quadripoint homomorphic_multiplication(const CurveBipoint& a, const TwistBipoint& b) const;

		CurveBipoint homomorphic_addition_no_rerandomize(const CurveBipoint& a, const CurveBipoint& b) const;
		TwistBipoint homomorphic_addition_no_rerandomize(const TwistBipoint& a, const TwistBipoint& b) const;
		Quadripoint homomorphic_addition_no_rerandomize(const Quadripoint& a, const Quadripoint& b) const;
		Quadripoint homomorphic_multiplication_no_rerandomize(const CurveBipoint& a, const TwistBipoint& b) const;

		CurveBipoint rerandomize(const CurveBipoint& a) const;
		TwistBipoint rerandomize(const TwistBipoint& a) const;
		Quadripoint rerandomize(const Quadripoint& a) const;
		CurveBipoint rerandomize(Scalar& lambda, const CurveBipoint& a) const;
		TwistBipoint rerandomize(Scalar& lambda, const TwistBipoint& a) const;

		CurveBipoint get_bipoint_curvegen() const;
		TwistBipoint get_bipoint_twistgen() const;	
		CurveBipoint get_bipoint_curve_subgroup_gen() const;
		TwistBipoint get_bipoint_twist_subgroup_gen() const;

		friend std::ostream& operator<<(std::ostream& os, const BGNPublicKey& output);
        friend std::istream& operator>>(std::istream& is, BGNPublicKey& input);
		
	private:
		void set(const CurveBipoint& g, const TwistBipoint& h, const CurveBipoint& g1, const TwistBipoint& h1);
		friend class BGN;

		CurveBipoint bipoint_curvegen; // g
		TwistBipoint bipoint_twistgen; // h
		CurveBipoint bipoint_curve_subgroup_gen; // (g^(a1), g^(b1))
		TwistBipoint bipoint_twist_subgroup_gen; // (h^(a2), h^(b2))
		Quadripoint quadripoint_subgroup_gen_a; // e(g, h1)
		Quadripoint quadripoint_subgroup_gen_b; // e(g1, h)
};

#endif
