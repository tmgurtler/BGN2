#ifndef __CURVEPOINT_HPP
#define __CURVEPOINT_HPP

#include <ostream>
#include <functional>

#include "Scalar.hpp"
#include "print_helpers.hpp"

extern "C" {
#include "curvepoint_fp.h"
}

class Curvepoint
{
    public:
        Curvepoint();
        Curvepoint(const curvepoint_fp_t input);

        curvepoint_fp_t& toCurvepointFpT();
        const curvepoint_fp_t& toCurvepointFpT() const;

        Curvepoint operator+(const Curvepoint& b) const;
        Curvepoint operator-(const Curvepoint& b) const;
        Curvepoint operator*(const Scalar& mult) const;

        bool operator==(const Curvepoint& b) const;
        bool operator<(const Curvepoint& b) const;
        bool operator>(const Curvepoint& b) const;
        bool operator<=(const Curvepoint& b) const;
        bool operator>=(const Curvepoint& b) const;
        bool operator!=(const Curvepoint& b) const;

        void make_affine();

        friend class CurvepointHash;
        friend class CurveBipoint;
        friend std::ostream& operator<<(std::ostream& os, const Curvepoint& output);
        friend std::istream& operator>>(std::istream& is, Curvepoint& input);
        
    private:
        curvepoint_fp_t point;
};

class CurvepointHash
{
    public:
        size_t operator()(const Curvepoint& x) const;
};

class Twistpoint
{
    public:
        Twistpoint();
        Twistpoint(const twistpoint_fp2_t input);

        twistpoint_fp2_t& toTwistpointFp2T();
        const twistpoint_fp2_t& toTwistpointFp2T() const;

        Twistpoint operator+(const Twistpoint& b) const;
        Twistpoint operator-(const Twistpoint& b) const;
        Twistpoint operator*(const Scalar& mult) const;

        bool operator==(const Twistpoint& b) const;
        bool operator<(const Twistpoint& b) const;
        bool operator>(const Twistpoint& b) const;
        bool operator<=(const Twistpoint& b) const;
        bool operator>=(const Twistpoint& b) const;
        bool operator!=(const Twistpoint& b) const;

        void make_affine();

        friend class TwistpointHash;
        friend class TwistBipoint;
        friend std::ostream& operator<<(std::ostream& os, const Twistpoint& output);
        friend std::istream& operator>>(std::istream& is, Twistpoint& input);
        
    private:
        twistpoint_fp2_t point;
};

class TwistpointHash
{
    public:
        size_t operator()(const Twistpoint& x) const;
};

#endif
