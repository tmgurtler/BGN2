#ifndef __BGN_HPP
#define __BGN_HPP

#include "Bipoint.hpp"
#include "Quadripoint.hpp"
#include "Scalar.hpp"
#include "PublicKey.hpp"
#include "PrivateKey.hpp"

class BGN
{
	public:
		BGN();

		void encrypt(CurveBipoint& G_element, const Scalar& cleartext) const;
		void encrypt(TwistBipoint& H_element, const Scalar& cleartext) const;
		void encrypt(CurveBipoint& G_element, TwistBipoint& H_element, const Scalar& cleartext) const;

		CurveBipoint homomorphic_addition(const CurveBipoint& a, const CurveBipoint& b) const;
		TwistBipoint homomorphic_addition(const TwistBipoint& a, const TwistBipoint& b) const;
		Quadripoint homomorphic_addition(const Quadripoint& a, const Quadripoint& b) const;
		Quadripoint homomorphic_multiplication(const CurveBipoint& a, const TwistBipoint& b) const;

        CurveBipoint homomorphic_addition_no_rerandomize(const CurveBipoint& a, const CurveBipoint& b) const;
        TwistBipoint homomorphic_addition_no_rerandomize(const TwistBipoint& a, const TwistBipoint& b) const;
        Quadripoint homomorphic_addition_no_rerandomize(const Quadripoint& a, const Quadripoint& b) const;
        Quadripoint homomorphic_multiplication_no_rerandomize(const CurveBipoint& a, const TwistBipoint& b) const;

		Scalar decrypt(const CurveBipoint& ciphertext);
        Scalar decrypt(const TwistBipoint& ciphertext);
        Scalar decrypt(const Quadripoint & ciphertext);

        CurveBipoint rerandomize(const CurveBipoint& a) const;
        TwistBipoint rerandomize(const TwistBipoint& a) const;
        Quadripoint rerandomize(const Quadripoint& a) const;

        const BGNPublicKey& get_public_key() const;
        const BGNPrivateKey& get_private_key() const;

        friend std::ostream& operator<<(std::ostream& os, const BGN& output);
        friend std::istream& operator>>(std::istream& is, BGN& input);

	private:
		BGNPublicKey public_key;
		BGNPrivateKey private_key;
};

#endif /* __BGN_HPP */
