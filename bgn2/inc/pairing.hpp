#ifndef __PAIRING_HPP
#define __PAIRING_HPP

#include "Quadripoint.hpp"
#include "Bipoint.hpp"

extern "C" {
#include "optate.h"
}

Quadripoint pairing(const CurveBipoint& op1, const TwistBipoint& op2);

#endif

