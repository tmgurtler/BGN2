#ifndef __QUADRIPOINT_HPP
#define __QUADRIPOINT_HPP

#include <functional>

#include "Scalar.hpp"
#include "print_helpers.hpp"

extern "C" {
#include "fp12e.h"
}

class Quadripoint
{
	public:
		Quadripoint();
		Quadripoint(const fp12e_t& p1, const fp12e_t& p2, const fp12e_t& p3, const fp12e_t& p4);

		fp12e_t& operator[](int n);
		const fp12e_t& operator[](int n) const;

		Quadripoint operator+(const Quadripoint& b) const;
		Quadripoint operator*(const Scalar& exp) const;
		
		bool operator==(const Quadripoint& b) const;
		bool operator!=(const Quadripoint& b) const;

		friend class QuadripointHash;
		friend std::ostream& operator<<(std::ostream& os, const Quadripoint& output);
		friend std::istream& operator>>(std::istream& is, Quadripoint& input);
	
	private:
		fp12e_t point[4];
};

class QuadripointHash
{
	public:
		size_t operator()(const Quadripoint& x) const;
};

#endif
