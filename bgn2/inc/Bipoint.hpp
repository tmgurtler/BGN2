#ifndef __BIPOINT_HPP
#define __BIPOINT_HPP

#include <ostream>
#include <functional>

#include "Scalar.hpp"
#include "Curvepoint.hpp"
#include "print_helpers.hpp"

extern "C" {
#include "curvepoint_fp.h"
#include "twistpoint_fp2.h"
}

class CurveBipoint
{
	public:
		CurveBipoint();
		CurveBipoint(curvepoint_fp_t p1, curvepoint_fp_t p2);
		CurveBipoint(const Curvepoint& p1, const Curvepoint& p2);

		curvepoint_fp_t& operator[](int n);
		const curvepoint_fp_t& operator[](int n) const;

		CurveBipoint operator+(const CurveBipoint& b) const;
		CurveBipoint operator-(const CurveBipoint& b) const;
		CurveBipoint operator*(const Scalar& mult) const;

		bool operator==(const CurveBipoint& b) const;
		bool operator!=(const CurveBipoint& b) const;

		void make_affine();

		friend class CurveBipointHash;
		friend std::ostream& operator<<(std::ostream& os, const CurveBipoint& output);
		friend std::istream& operator>>(std::istream& is, CurveBipoint& input);
	
	private:
		curvepoint_fp_t point[2];

		bool equal(const curvepoint_fp_t& op1, const curvepoint_fp_t& op2) const;
};

class CurveBipointHash
{
	public:
		size_t operator()(const CurveBipoint& x) const;
};

class TwistBipoint
{
	public:
		TwistBipoint(); 
		TwistBipoint(twistpoint_fp2_t p1, twistpoint_fp2_t p2);

		twistpoint_fp2_t& operator[](int n);
		const twistpoint_fp2_t& operator[](int n) const;

		TwistBipoint operator+(const TwistBipoint& b) const;
		TwistBipoint operator-(const TwistBipoint& b) const;
		TwistBipoint operator*(const Scalar& mult) const;

		bool operator==(const TwistBipoint& b) const;
		bool operator!=(const TwistBipoint& b) const;

		void make_affine();

		friend class TwistBipointHash;
		friend std::ostream& operator<<(std::ostream& os, const TwistBipoint& output);
		friend std::istream& operator>>(std::istream& is, TwistBipoint& input);
	
	private:
		twistpoint_fp2_t point[2];	

		bool equal(const twistpoint_fp2_t& op1, const twistpoint_fp2_t& op2) const;
};

class TwistBipointHash
{
	public:
		size_t operator()(const TwistBipoint& x) const;
};

#endif
