#include "Quadripoint.hpp"

Quadripoint::Quadripoint()
{
	fp12e_setone(point[0]);
	fp12e_setone(point[1]);
	fp12e_setone(point[2]);
	fp12e_setone(point[3]);	
}

Quadripoint::Quadripoint(const fp12e_t& p1, const fp12e_t& p2, const fp12e_t& p3, const fp12e_t& p4)
{
	fp12e_set(point[0], p1);
	fp12e_set(point[1], p2);
	fp12e_set(point[2], p3);
	fp12e_set(point[3], p4);
}

fp12e_t& Quadripoint::operator[](int n)
{
	return point[n];
}

const fp12e_t& Quadripoint::operator[](int n) const
{
	return point[n];
}

Quadripoint Quadripoint::operator+(const Quadripoint& b) const
{
	Quadripoint retval;

	if (fp12e_iseq(point[0], b[0]))
		fp12e_square(retval[0], point[0]);
	else
		fp12e_mul(retval[0], point[0], b[0]);

	if (fp12e_iseq(point[1], b[1]))
		fp12e_square(retval[1], point[1]);
	else
		fp12e_mul(retval[1], point[1], b[1]);

	if (fp12e_iseq(point[2], b[2]))
		fp12e_square(retval[2], point[2]);
	else
		fp12e_mul(retval[2], point[2], b[2]);

	if (fp12e_iseq(point[3], b[3]))
		fp12e_square(retval[3], point[3]);
	else
		fp12e_mul(retval[3], point[3], b[3]);

	return retval;
}

Quadripoint Quadripoint::operator*(const Scalar& exp) const
{
	Quadripoint retval;

	if (exp == Scalar(0))
	{
		fp12e_setone(retval[0]);
		fp12e_setone(retval[1]);
		fp12e_setone(retval[2]);
		fp12e_setone(retval[3]);
	} else {
		exp.mult(retval[0], point[0]);
		exp.mult(retval[1], point[1]);
		exp.mult(retval[2], point[2]);
		exp.mult(retval[3], point[3]);
	}

	return retval;	
}

bool Quadripoint::operator==(const Quadripoint& b) const
{
	bool retval;
	
	retval =           fp12e_iseq(point[0], b[0]);
	retval = retval && fp12e_iseq(point[1], b[1]);
	retval = retval && fp12e_iseq(point[2], b[2]);
	retval = retval && fp12e_iseq(point[3], b[3]);
	
	return retval;
}

bool Quadripoint::operator!=(const Quadripoint& b) const
{
	return !(*this == b);
}

std::ostream& operator<<(std::ostream& os, const Quadripoint& output)
{
	for (int i = 0; i < 4; i++)
		os << Fp12e(output[i]);

	return os;
}

std::istream& operator>>(std::istream& is, Quadripoint& input)
{
	Fp12e realIn[4];
	
	for (int i = 0; i < 4; i++)
	{
		is >> realIn[i];
		fp12e_set(input[i], realIn[i].data);
	}

	return is;
}

size_t QuadripointHash::operator()(const Quadripoint& x) const
{
	size_t retval = 0;
	std::hash<double> hasher;

	for (int i = 0; i < 4; i++)
	{
		for (int j = 0; j < 24; j++)
		{
			/* I'm so sorry for pointer hell here,
			 * the types are just like this */

			retval ^= hasher(x[i]->m_a->m_a->v[j]);
			retval ^= hasher(x[i]->m_a->m_b->v[j]);
			retval ^= hasher(x[i]->m_a->m_c->v[j]);

			retval ^= hasher(x[i]->m_b->m_a->v[j]);
			retval ^= hasher(x[i]->m_b->m_b->v[j]);
			retval ^= hasher(x[i]->m_b->m_c->v[j]);
		}
	}

	return retval;
}
