#include "print_helpers.hpp"

std::ostream& operator<<(std::ostream& os, const BinarySizeT& output)
{
    size_t outval = output.val();
    os.write(reinterpret_cast<const char*>(&outval), sizeof(outval));

    return os;
}

std::istream& operator>>(std::istream& is, BinarySizeT& input)
{
    size_t inval;
    is.read(reinterpret_cast<char*>(&inval), sizeof(inval));

    input.data = inval;

    return is;
}

std::ostream& operator<<(std::ostream& os, const BinaryBool& output)
{
    uint8_t outval = (output.val() ? 1 : 0);
    os.write(reinterpret_cast<const char*>(&outval), sizeof(outval));

    return os;
}

std::istream& operator>>(std::istream& is, BinaryBool& input)
{
    uint8_t inval;
    is.read(reinterpret_cast<char*>(&inval), sizeof(inval));

    input.data = (inval > 0 ? true : false);

    return is;
}

Fp12e::Fp12e(const fp12e_t& input)
{
    fp12e_set(data, input);
}

Fp12e::Fp12e()
{
    fp12e_setzero(data);
}

Fp6e::Fp6e(const fp6e_t& input)
{
    fp6e_set(data, input);
}

Fp6e::Fp6e()
{
    fp6e_setzero(data);
}

Fp2e::Fp2e(const fp2e_t& input)
{
    fp2e_set(data, input);
}

Fp2e::Fp2e()
{
    fp2e_setzero(data);
}

Fpe::Fpe(const fpe_t& input)
{
    fpe_set(data, input);
}

Fpe::Fpe()
{
    fpe_setzero(data);
}

std::ostream& operator<<(std::ostream& os, const Fp12e& output)
{
    os << Fp6e(output.data->m_a) << Fp6e(output.data->m_b);

    return os;
}

std::istream& operator>>(std::istream& is, Fp12e& input)
{
    Fp6e a, b;
    is >> a >> b;

    fp6e_set(input.data->m_a, a.data);
    fp6e_set(input.data->m_b, b.data);

    return is;
}

std::ostream& operator<<(std::ostream& os, const Fp6e& output)
{
    os << Fp2e(output.data->m_a) << Fp2e(output.data->m_b) << Fp2e(output.data->m_c);

    return os;
}

std::istream& operator>>(std::istream& is, Fp6e& input)
{
    Fp2e a, b, c;
    is >> a >> b >> c;

    fp2e_set(input.data->m_a, a.data);
    fp2e_set(input.data->m_b, b.data);
    fp2e_set(input.data->m_c, c.data);

    return is;
}

std::ostream& operator<<(std::ostream& os, const Fp2e& output)
{
    fpe_t a, b;
    fp2e_to_2fpe(a, b, output.data);

    os << Fpe(a) << Fpe(b);

    return os;
}

std::istream& operator>>(std::istream& is, Fp2e& input)
{
    Fpe a, b;
    is >> a >> b;
    _2fpe_to_fp2e(input.data, a.data, b.data);

    return is;
}

std::ostream& operator<<(std::ostream& os, const Fpe& output)
{
    if (os.flags() & std::ios::hex)
    {
        for (int i = 0; i < 12; i++)
            hex_double(os, output.data->v[i]);
    }
    else
    {
        for (int i = 0; i < 12; i++)
            raw_double(os, output.data->v[i]);
    }

    return os;
}

std::istream& operator>>(std::istream& is, Fpe& input)
{
    if (is.flags() & std::ios::hex)
    {
        for (int i = 0; i < 12; i++)
            hex_double(is, input.data->v[i]);
    }
    else
    {
        for (int i = 0; i < 12; i++)
            raw_double(is, input.data->v[i]);
    }
    
    return is;       
}

std::ostream& hex_double(std::ostream& os, double d)
{
    os << *reinterpret_cast<const unsigned long long*>(&d);

    return os;
}

std::istream& hex_double(std::istream& is, double& d)
{ 
    is >> *reinterpret_cast<unsigned long long*>(&d);

    return is;
}

std::ostream& raw_double(std::ostream& os, double d)
{
    os.write(reinterpret_cast<const char*>(&d), sizeof(d));

    return os;
}

std::istream& raw_double(std::istream& is, double& d)
{ 
    is.read(reinterpret_cast<char*>(&d), sizeof(d));

    return is;
}
