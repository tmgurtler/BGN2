#include "pairing.hpp"

Quadripoint pairing(const CurveBipoint& op1, const TwistBipoint& op2)
{
    CurveBipoint affine_op1 = op1;
    TwistBipoint affine_op2 = op2;

    affine_op1.make_affine();
    affine_op2.make_affine();
    
	Quadripoint retval;

	optate(retval[0], affine_op2[0], affine_op1[0]);
	optate(retval[1], affine_op2[1], affine_op1[0]);
	optate(retval[2], affine_op2[0], affine_op1[1]);
	optate(retval[3], affine_op2[1], affine_op1[1]);
	
    return retval;
}
