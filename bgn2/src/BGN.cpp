#include "BGN.hpp"

extern const curvepoint_fp_t bn_curvegen;
extern const twistpoint_fp2_t bn_twistgen;

BGN::BGN()
{
	Scalar a1, b1, c1, d1, a2, b2, c2, d2;
    
    while (true)
    {
        a1.set_field_random();
        b1.set_field_random();
        c1.set_field_random();

        if (a1 != Scalar(0))
        {
            d1 = (b1.fieldMult(c1).fieldAdd(Scalar(1))).fieldMult(a1.fieldMultInverse());
            break;
        }
    }

    while (true)
    {
        a2.set_field_random();
        b2.set_field_random();
        c2.set_field_random();

        if (a2 != Scalar(0))
        {
            d2 = (b2.fieldMult(c2).fieldAdd(Scalar(1))).fieldMult(a2.fieldMultInverse());
            break;
        }   
    }

    Scalar r1, r2, r3, r4;
    r1.set_random();
    r2.set_random();
    r3.set_random();
    r4.set_random();

    curvepoint_fp_t g_part1, g_part2, g_a1, g_b1;
    
    r1.mult(g_part1, bn_curvegen);
    r2.mult(g_part2, bn_curvegen);
    a1.mult(g_a1, bn_curvegen);
    b1.mult(g_b1, bn_curvegen);

    CurveBipoint full_g(g_part1, g_part2);
    CurveBipoint full_g1(g_a1, g_b1);

    twistpoint_fp2_t h_part1, h_part2, h_a2, h_b2;

    r3.mult(h_part1, bn_twistgen);
    r4.mult(h_part2, bn_twistgen);
    a2.mult(h_a2, bn_twistgen);
    b2.mult(h_b2, bn_twistgen);
    
    TwistBipoint full_h(h_part1, h_part2);
    TwistBipoint full_h1(h_a2, h_b2);

    full_g.make_affine();
    full_g1.make_affine();
    full_h.make_affine();
    full_h1.make_affine();
        
    public_key.set(full_g, full_h, full_g1, full_h1);
    private_key.set(public_key, a1, b1, c1, d1, a2, b2, c2, d2);
}

void BGN::encrypt(CurveBipoint& G_element, const Scalar& cleartext) const
{
	public_key.encrypt(G_element, cleartext);
}

void BGN::encrypt(TwistBipoint& H_element, const Scalar& cleartext) const
{
	public_key.encrypt(H_element, cleartext);
}

void BGN::encrypt(CurveBipoint& G_element, TwistBipoint& H_element, const Scalar& cleartext) const
{
	public_key.encrypt(G_element, H_element, cleartext);
}

CurveBipoint BGN::homomorphic_addition(const CurveBipoint& a, const CurveBipoint& b) const
{
	return public_key.homomorphic_addition(a, b);
}

TwistBipoint BGN::homomorphic_addition(const TwistBipoint& a, const TwistBipoint& b) const
{
	return public_key.homomorphic_addition(a, b);
}

Quadripoint BGN::homomorphic_addition(const Quadripoint& a, const Quadripoint& b) const
{
	return public_key.homomorphic_addition(a, b);
}

Quadripoint BGN::homomorphic_multiplication(const CurveBipoint& a, const TwistBipoint& b) const
{
	return public_key.homomorphic_multiplication(a, b);
}

CurveBipoint BGN::homomorphic_addition_no_rerandomize(const CurveBipoint& a, const CurveBipoint& b) const
{
    return public_key.homomorphic_addition_no_rerandomize(a, b);
}

TwistBipoint BGN::homomorphic_addition_no_rerandomize(const TwistBipoint& a, const TwistBipoint& b) const
{
    return public_key.homomorphic_addition_no_rerandomize(a, b);
}

Quadripoint BGN::homomorphic_addition_no_rerandomize(const Quadripoint& a, const Quadripoint& b) const
{
    return public_key.homomorphic_addition_no_rerandomize(a, b);
}

Quadripoint BGN::homomorphic_multiplication_no_rerandomize(const CurveBipoint& a, const TwistBipoint& b) const
{
    return public_key.homomorphic_multiplication_no_rerandomize(a, b);
}

CurveBipoint BGN::rerandomize(const CurveBipoint& G_element) const
{
    return public_key.rerandomize(G_element);
}

TwistBipoint BGN::rerandomize(const TwistBipoint& H_element) const
{
    return public_key.rerandomize(H_element);
}

Quadripoint BGN::rerandomize(const Quadripoint& Gt_element) const
{
    return public_key.rerandomize(Gt_element);
}

Scalar BGN::decrypt(const CurveBipoint& ciphertext)
{
	return private_key.decrypt(ciphertext);
}

Scalar BGN::decrypt(const TwistBipoint& ciphertext)
{
	return private_key.decrypt(ciphertext);
}

Scalar BGN::decrypt(const Quadripoint& ciphertext)
{
	return private_key.decrypt(ciphertext);
}

const BGNPublicKey& BGN::get_public_key() const
{
	return public_key;
}

const BGNPrivateKey& BGN::get_private_key() const
{
    return private_key;
}

std::ostream& operator<<(std::ostream& os, const BGN& output)
{
    os << output.public_key;
    os << output.private_key;

    return os;
}

std::istream& operator>>(std::istream& is, BGN& input)
{
    is >> input.public_key;
    is >> input.private_key;

    return is;
}
