#include "PublicKey.hpp"

BGNPublicKey::BGNPublicKey(const BGNPublicKey& other)
    : bipoint_curvegen(other.bipoint_curvegen), bipoint_twistgen(other.bipoint_twistgen),
    bipoint_curve_subgroup_gen(other.bipoint_curve_subgroup_gen),
    bipoint_twist_subgroup_gen(other.bipoint_twist_subgroup_gen)
{ }

void BGNPublicKey::encrypt(CurveBipoint& G_element, const Scalar& cleartext) const
{
    Scalar lambda;
    lambda.set_random();

    CurveBipoint cleartext_as_element, random_mask;
    cleartext_as_element = get_bipoint_curvegen() * cleartext;
    random_mask = get_bipoint_curve_subgroup_gen() * lambda;

    G_element = cleartext_as_element + random_mask;
}

CurveBipoint BGNPublicKey::curveEncrypt(Scalar& lambda, const Scalar& cleartext) const
{
    lambda.set_random();

    CurveBipoint cleartext_as_element, random_mask;
    cleartext_as_element = get_bipoint_curvegen() * cleartext;
    random_mask = get_bipoint_curve_subgroup_gen() * lambda;

    return cleartext_as_element + random_mask;
}

TwistBipoint BGNPublicKey::twistEncrypt(Scalar& lambda, const Scalar& cleartext) const
{
    lambda.set_random();

    TwistBipoint cleartext_as_element, random_mask;
    cleartext_as_element = get_bipoint_twistgen() * cleartext;
    random_mask = get_bipoint_twist_subgroup_gen() * lambda;

    return cleartext_as_element + random_mask;
}

void BGNPublicKey::encrypt(TwistBipoint& H_element, const Scalar& cleartext) const
{
    Scalar lambda;
    lambda.set_random();

    TwistBipoint cleartext_as_element, random_mask;
    cleartext_as_element = get_bipoint_twistgen() * cleartext;
    random_mask = get_bipoint_twist_subgroup_gen() * lambda;

    H_element = cleartext_as_element + random_mask;
}

void BGNPublicKey::encrypt(CurveBipoint& G_element, TwistBipoint& H_element, const Scalar& cleartext) const
{
    encrypt(G_element, cleartext);
    encrypt(H_element, cleartext);
}

CurveBipoint BGNPublicKey::homomorphic_addition(const CurveBipoint& a, const CurveBipoint& b) const
{
    return rerandomize(homomorphic_addition_no_rerandomize(a, b));
}

TwistBipoint BGNPublicKey::homomorphic_addition(const TwistBipoint& a, const TwistBipoint& b) const
{
    return rerandomize(homomorphic_addition_no_rerandomize(a, b));
}

Quadripoint BGNPublicKey::homomorphic_addition(const Quadripoint& a, const Quadripoint& b) const
{
    return rerandomize(homomorphic_addition_no_rerandomize(a, b));
}

Quadripoint BGNPublicKey::homomorphic_multiplication(const CurveBipoint& a, const TwistBipoint& b) const
{
    return rerandomize(homomorphic_multiplication_no_rerandomize(a, b));
}

CurveBipoint BGNPublicKey::homomorphic_addition_no_rerandomize(const CurveBipoint& a, const CurveBipoint& b) const
{
    return a + b;
}

TwistBipoint BGNPublicKey::homomorphic_addition_no_rerandomize(const TwistBipoint& a, const TwistBipoint& b) const
{
    return a + b;
}

Quadripoint BGNPublicKey::homomorphic_addition_no_rerandomize(const Quadripoint& a, const Quadripoint& b) const
{
    return a + b;
}

Quadripoint BGNPublicKey::homomorphic_multiplication_no_rerandomize(const CurveBipoint& a, const TwistBipoint& b) const
{
    return pairing(a, b);
}

CurveBipoint BGNPublicKey::rerandomize(const CurveBipoint& a) const
{
    Scalar lambda;
    lambda.set_random();

    CurveBipoint random_mask;
    random_mask = bipoint_curve_subgroup_gen * lambda;

    return a + random_mask;
}

TwistBipoint BGNPublicKey::rerandomize(const TwistBipoint& a) const
{
    Scalar lambda;
    lambda.set_random();

    TwistBipoint random_mask;
    random_mask = bipoint_twist_subgroup_gen * lambda;

    return a + random_mask;
}

Quadripoint BGNPublicKey::rerandomize(const Quadripoint& a) const
{
    Scalar lambda1, lambda2;
    lambda1.set_random();
    lambda2.set_random();

    Quadripoint random_mask;
    random_mask = quadripoint_subgroup_gen_a * lambda1 + quadripoint_subgroup_gen_b * lambda2;

    return a + random_mask;
}

CurveBipoint BGNPublicKey::rerandomize(Scalar& lambda, const CurveBipoint& a) const
{
    lambda.set_random();

    CurveBipoint random_mask;
    random_mask = bipoint_curve_subgroup_gen * lambda;

    return a + random_mask;
}

TwistBipoint BGNPublicKey::rerandomize(Scalar& lambda, const TwistBipoint& a) const
{
    lambda.set_random();

    TwistBipoint random_mask;
    random_mask = bipoint_twist_subgroup_gen * lambda;

    return a + random_mask;
}

CurveBipoint BGNPublicKey::get_bipoint_curvegen() const
{
    return bipoint_curvegen;
}

TwistBipoint BGNPublicKey::get_bipoint_twistgen() const
{
    return bipoint_twistgen;
}

CurveBipoint BGNPublicKey::get_bipoint_curve_subgroup_gen() const
{
    return bipoint_curve_subgroup_gen;
}

TwistBipoint BGNPublicKey::get_bipoint_twist_subgroup_gen() const
{
    return bipoint_twist_subgroup_gen;
}

std::ostream& operator<<(std::ostream& os, const BGNPublicKey& output)
{
    os << output.bipoint_curvegen;
    os << output.bipoint_twistgen;
    os << output.bipoint_curve_subgroup_gen;
    os << output.bipoint_twist_subgroup_gen;

    return os;
}

std::istream& operator>>(std::istream& is, BGNPublicKey& input)
{
    is >> input.bipoint_curvegen;
    is >> input.bipoint_twistgen;
    is >> input.bipoint_curve_subgroup_gen;
    is >> input.bipoint_twist_subgroup_gen;

    input.quadripoint_subgroup_gen_a = pairing(input.bipoint_curvegen, input.bipoint_twist_subgroup_gen);
    input.quadripoint_subgroup_gen_b = pairing(input.bipoint_curve_subgroup_gen, input.bipoint_twistgen);

    return is;
}

BGNPublicKey::BGNPublicKey()
{ }

void BGNPublicKey::set(const CurveBipoint& g, const TwistBipoint& h, const CurveBipoint& g1, const TwistBipoint& h1)
{
    bipoint_curvegen = g;
    bipoint_twistgen = h;
    
    bipoint_curve_subgroup_gen = g1;
    bipoint_twist_subgroup_gen = h1;

    quadripoint_subgroup_gen_a = pairing(g, h1);
    quadripoint_subgroup_gen_b = pairing(g1, h);
}
